arr = [7,5,3]
arr_2 = [3,5,1]

def cek_can_sum(arr, x): # arr = [1,3,5], x = 5
    if x < 0:
        return False

    table_init = [True] + [-1 for i in range(x)]
    arr.sort()

    for i in range(len(table_init)):
        if i >= arr[0]: # min(arr) = 1 so i = 1,2,3,4,5
            cek_can_sum_recursive(arr, table_init, i)

    print(f"\n#################### {x} #####################\n")
    print(table_init)
    return table_init[x]

def cek_can_sum_recursive(arr, table, i):
    if (len(arr) == 0):
        return False
    
    if (i >= 0):
        if (table[i] == -1):
            table[i] = False or cek_can_sum_recursive(arr, table, i - arr[0]) or cek_can_sum_recursive(arr[1:], table, i)
    
        return table[i]


print(cek_can_sum(arr, 8))
print(cek_can_sum(arr, 4))
print(cek_can_sum(arr, 100))
print(cek_can_sum(arr, -2))
print(cek_can_sum(arr, -100))
print(cek_can_sum(arr, 0))
print(cek_can_sum([5,30,31], 32))
print(cek_can_sum([5,30,31], 95))
print(cek_can_sum(arr_2, 5))