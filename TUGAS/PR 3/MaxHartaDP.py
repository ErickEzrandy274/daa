arr = [ 5, 5, 10, 100, 10, 5 ]
arr2 = [3,2,2,3,5,6]

def findMaxSum(arr, N):
    # Declare dp array
    table = [[0 for i in range(3)] for j in range(N)]
     
    if (N == 1):
        return arr[0]
   
    # Initialize the values in dp array
    table[0][1] = arr[0]
    table[0][2] = 1
   
    # Loop to find the maximum possible sum
    for i in range(1,N):
        table[i][1] = table[i - 1][0] + arr[i]
        table[i][0] = max(table[i - 1][1], table[i - 1][0])

        if table[i][1] > table[i][0]:
          table[i][2] = i-1
        else:
          table[i][2] = i
   
    # Return the maximum sum
    print(table)
    return f"Harta maximum: {max(table[N - 1][0], table[N - 1][1])} dengan melalui bilik {table[N - 1][2]}"
 
print(findMaxSum(arr2, len(arr2)))