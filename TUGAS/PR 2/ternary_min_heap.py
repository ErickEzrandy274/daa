def LEFT(i):
  return (3*i) + 1

def MID(i):
  return (3*i) + 2

def RIGHT(i):
  return (3*i) + 3

def PARENT(i):
  return (i-1) // 3

def ternary_min_heap(A,i):
  left = LEFT(i)
  mid = MID(i)
  right = RIGHT(i)
  minimum = i

  if left < len(A) and A[left] < A[i]:
    minimum = left

  if mid < len(A) and A[mid] < A[minimum]:
    minimum = mid

  if right < len(A) and A[right] < A[minimum]:
    minimum = right

  if minimum != i:
    A[i], A[minimum] = A[minimum], A[i]
    ternary_min_heap(A, minimum)

def build_ternary_min_heap(A):
  n = (len(A) - 2) // 3

  for i in range(n, -1, -1):
    ternary_min_heap(A, i)

  print(A)

A = [5, 6, 3, 2, 4, 1, 7]
build_ternary_min_heap(A)