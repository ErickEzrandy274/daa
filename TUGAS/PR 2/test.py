def rekursi(n):
    if (n > 0):
        rekursi(n // 3)
        for i in range(1,n):
            print('O', end='')
        rekursi((2*n) // 3)

# rekursi(1) # 0
# print("\n##############")
# rekursi(2) # 1
# print("\n##############")
# rekursi(3) # 3
# print("\n##############")
# rekursi(4) # 4
# print("\n##############")
# rekursi(5) # 7
# print("\n##############")
# rekursi(6) # 10
# print("\n##############")
# rekursi(7) # 11
# print("\n##############")
# rekursi(8) # 15
# print("\n##############")
# rekursi(9) # 19
# print("\n##############")
# rekursi(10) # 23
a = [1,2,3,4,5,6,7,8,9,10]
n = (len(a) - 2) // 3
for i in range(n, -1, -1):
    print(i)